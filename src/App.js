import './App.css';
import Header from './Components/Header';
import Main from './Components/Main';
import { BrowserRouter, Route } from 'react-router-dom'
import Detailedpage from './Components/Detailedpage';
// import { Router } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <Header />
      <Route exact path='/' component={Main}/>
      <Route exact path='/:venkat' component={Detailedpage}/>
    </BrowserRouter>
  );
}

export default App;
