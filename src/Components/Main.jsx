import React, { Component } from 'react'
import axios from 'axios'
import Input from './Input'
import Filter from './Filter'
// import Detailedpage from './Components/Detailedpage';

import {Link} from 'react-router-dom'

class Main extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         posts:[],
         postsCopy:[]
      }
    }
    componentDidMount(){
        axios.get("https://restcountries.com/v3.1/all")
        .then(response =>{
            console.log(response)
            this.setState({posts:response.data,postsCopy:response.data})
        })
        .catch(error =>{
            console.log(error);
        })
    }

  handleCountry =(e)=>{
    const filteredCountries= this.state.posts.filter((each)=>
    each.name.common.toLowerCase().includes(e.target.value.toLowerCase())
    );
    console.log();
    this.setState({postsCopy:filteredCountries})
  }


  handleRegion =(e)=>{
    if(!(e.target.value)){
      this.setState({postsCopy:this.state.posts})
    }else{
      const region =this.state.posts.filter((each)=>
    each.region === e.target.value 
    );  
    this.setState({postsCopy:region})
    }
    
  }


  render() {

    const {postsCopy} = this.state
    // console.log(postsCopy);
    return (
    <div className='mainContainer'>
    <Input handleCountry={this.handleCountry} />
    <Filter regionChange={this.handleRegion}/>
    <div className='Container'>
      <div className='CountryCard'>
        {
          postsCopy.length? postsCopy.map(post=> {
              return(
                <div className='total'>
                <Link to={`/${post.name.common}`}><img className="flag" src={post.flags.png} alt=""/></Link>
                <div className='content'><strong> {post.name.common}</strong></div>
                <div className='para'>
                <div><strong>Population:</strong> {post.population}</div>
                <div> <strong>Region:</strong> {post.region}</div>
                <div> <strong>Capital:</strong> {post.capital}</div></div>
                </div>
              )
          }):<h2> Loading... Please Wait</h2>
        }</div>
        </div>
        </div>
      
    )
  }
}

export default Main