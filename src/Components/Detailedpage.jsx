import React, { Component } from 'react';
import axios from 'axios'
import {Link} from 'react-router-dom'


class Detailedpage extends Component {
    constructor(props) {
        super(props)
      
        this.state = {
           posts:[],
        }
      }
      componentDidMount(){
          axios.get(`https://restcountries.com/v3.1/name${this.props.match.url}`)
          .then(response =>{
              console.log(response)
              this.setState({posts:response.data,postsCopy:response.data})
          })
          .catch(error =>{
              console.log(error);
          })
      }
    render() { 
        const {posts}=this.state
        console.log(this.props)
        return (
            
            <div >
            <Link to={"/"}><button className='btn'>Back</button></Link>
            {
            posts.length? posts.map(post =>{
                return (
                    <div className='newComponent'>
                        <div>
                           <img className='newimage' src={post.flags.png}  alt="" />
                        </div>
                        <div className='sidecontainer'>
                        <h1 className='newheader'>{post.name.common} </h1>
                        <div className='newlist'>
                            {/* <p>Native Name>{post.name.nativeName}></p> */}
                            <p><strong>Population: </strong> {post.population} </p>
                            <p><strong>Region: </strong>{post.region}</p>
                            <p><strong>Sub Region: </strong>{post.subregion}</p>
                            <p><strong>Capital: </strong>{post.capital}</p>
                            <p><strong>Top Level Domain: </strong>{post.tld}</p>
                            <p><strong>Currencies: </strong>{post.currencies[0]}</p>                
                            <p><strong>Languages: </strong>{post.languages[0]}</p>
                            <p><strong>Border Countries: </strong> {post.borders.map((border) => {
                            return (<button className='boundaries'>{border}</button>)})}</p>
                        </div>
                        </div>
                    </div>
                )
            }):<h2> Loading... Please Wait</h2>
            }    
            </div>
        );
    }
}
 
export default Detailedpage;