import React, { Component } from 'react'

class Filter extends Component {
  render() {
    return (
      <div>
          <select className='dropdown' onChange={this.props.regionChange} >
            <option value="">Filter by Region</option>
            <option value="Africa">Africa</option>
            <option value="Americas">Americas</option>
            <option value="Asia">Asia</option>
            <option value="Europe">Europe</option>
            <option value="Oceania">Oceania</option>
        </select>
      </div>
    )
  }
}

export default Filter