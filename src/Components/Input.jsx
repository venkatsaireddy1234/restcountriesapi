import React, { Component } from 'react'

 class Input extends Component {

  render() {
    return (
      <div>
        <input onChange={this.props.handleCountry} type="text" placeholder='search for a country...' className='input'></input>
      </div>
    )
  }
}

export default Input