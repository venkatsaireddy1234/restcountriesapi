import React, { Component } from 'react'

class Header extends Component {
  render() {
    return (
      <div className='header'>
        <header className='where'>Where in the world?</header>
        <p className='mode'>Dark Mode</p>
        <br></br>
      </div>
      
    )
  }
}

export default Header